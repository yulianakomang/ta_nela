/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

DROP TABLE IF EXISTS `tb_admin`;
CREATE TABLE `tb_admin` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nama` varchar(128) DEFAULT NULL,
  `alamat` varchar(254) DEFAULT NULL,
  `username` char(128) DEFAULT NULL,
  `password` varchar(128) DEFAULT NULL,
  `no_telp` varchar(20) DEFAULT NULL,
  `level` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `tb_detail_penjualan`;
CREATE TABLE `tb_detail_penjualan` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nomor_faktur` char(16) NOT NULL DEFAULT '',
  `id_produk` int(11) unsigned DEFAULT NULL,
  `jumlah` int(11) NOT NULL DEFAULT '0',
  `subtotal` decimal(12,2) DEFAULT NULL,
  `total_pokok` decimal(12,2) DEFAULT NULL,
  `tgl_order` date DEFAULT NULL,
  `satuan` varchar(50) DEFAULT NULL,
  `harga_jual` decimal(12,2) NOT NULL,
  `harga_pokok` decimal(12,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `tb_kategori`;
CREATE TABLE `tb_kategori` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nama_kategori` varchar(128) NOT NULL DEFAULT '',
  `deskripsi` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `tb_pelanggan`;
CREATE TABLE `tb_pelanggan` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `kode` varchar(100) DEFAULT NULL,
  `nama` varchar(128) DEFAULT '',
  `no_telp` char(14) DEFAULT NULL,
  `alamat` char(128) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `tb_pembayaran`;
CREATE TABLE `tb_pembayaran` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nomor_faktur` char(16) NOT NULL,
  `tanggal_pembayaran` date DEFAULT NULL,
  `sisa_pembayaran` decimal(10,2) DEFAULT NULL,
  `kode` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `tb_penjualan`;
CREATE TABLE `tb_penjualan` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nomor_faktur` char(16) NOT NULL DEFAULT '',
  `id_admin` int(10) unsigned NOT NULL,
  `id_pelanggan` int(11) unsigned DEFAULT NULL,
  `tanggal_order` date DEFAULT NULL,
  `tanggal_terima` date DEFAULT NULL,
  `uang_muka` decimal(10,2) DEFAULT NULL,
  `total` decimal(12,2) DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  `sisa` decimal(12,2) DEFAULT NULL,
  PRIMARY KEY (`nomor_faktur`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `tb_produk`;
CREATE TABLE `tb_produk` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `kode_produk` varchar(100) DEFAULT NULL,
  `nama` varchar(128) DEFAULT NULL,
  `harga` decimal(10,2) NOT NULL DEFAULT '0.00',
  `gambar` varchar(128) DEFAULT NULL,
  `satuan` char(10) DEFAULT NULL,
  `harga_jual` decimal(10,2) NOT NULL DEFAULT '0.00',
  `kategori_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

INSERT INTO `tb_admin` (`alamat`,`password`,`id`,`level`,`username`,`nama`,`no_telp`) VALUES 
 ('Jl. Sistem, No. 500', '21232f297a57a5a743894a0e4a801fc3', '1', '1', 'admin', 'I Wayan Admin', '081299881988'),
('Jl. Server, No. 404', 'ee11cbb19052e40b07aac0ca060c23ee', '2', '0', 'user', 'I Wayan User', '08888888888888');

INSERT INTO `tb_detail_penjualan` (`jumlah`,`total_pokok`,`tgl_order`,`id`,`subtotal`,`id_produk`,`satuan`,`harga_jual`,`nomor_faktur`,`harga_pokok`) VALUES 
 ('1', '80000.00', '2019-10-20', '45', '100000.00', '3', 'pcs', '100000.00', 'PJ-1571567406', '80000.00'),
('4', '4000.00', '2019-10-20', '46', '8000.00', '6', 'kg', '2000.00', 'PJ-1571567406', '1000.00');

INSERT INTO `tb_kategori` (`id`,`nama_kategori`,`deskripsi`) VALUES 
 ('2', 'undangan', 'kartu undangan'),
('3', 'udang2', 'keju');

INSERT INTO `tb_pelanggan` (`id`,`no_telp`,`kode`,`alamat`,`nama`) VALUES 
 ('4', '085737422503', 'M1234', 'asdfasdfa', 'Komang Gede Yuliana'),
('5', '085737422503', 'PLG-1571557524', '123', 'Komang Gede Yuliana');

INSERT INTO `tb_penjualan` (`status`,`id`,`tanggal_order`,`tanggal_terima`,`id_admin`,`nomor_faktur`,`id_pelanggan`,`uang_muka`,`total`,`sisa`) VALUES 
 ('0.00', '26', '2019-10-20', '2019-10-20', '2', 'PJ-1571567406', '4', '0.00', '108000.00', '108000.00');

INSERT INTO `tb_produk` (`gambar`,`satuan`,`id`,`kode_produk`,`harga`,`nama`,`harga_jual`,`kategori_id`) VALUES 
 ('macbook pro 2012 SSD.jpeg', 'kg', '2', 'XXXX', '1000.00', 'XXXX', '2000.00', '2'),
('default.jpg', 'pcs', '3', 'Asus', '80000.00', 'Asus ROG', '100000.00', '2'),
('A12.jpeg', 'kg', '6', 'A12', '1000.00', 'Kartu', '2000.00', '2');

/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;