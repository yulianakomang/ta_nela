<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('data_model', 'data');
	}

	public function index(){
		if ($this->session->userdata('username') != null) {
			redirect('/admin');
		}else 
			$this->load->view('auth/login');
	}

	public function login(){
		$login = $this->input->post();
		$admin = $this->data->get_admin($login['username'],$login['password']);

		print_r(count($admin->result()));

		if (count($admin->result()) > 0) {
			$data = $admin->row();
			$this->session->set_userdata(array(
				'id' => $data->id,
				'username' => $data->username,
				'nama' => $data->nama,
				'level' => $data->level,
			));
			redirect('admin');
		}else{
			$this->session->set_flashdata([
				'error' => true,
				'message' => 'Username tidak ditemukan atau password salah!'
			]);
			redirect('/');
		}
		// print_r($this->session);
	}

	public function logout(){
		$this->session->sess_destroy();
		redirect('login');
	}

}