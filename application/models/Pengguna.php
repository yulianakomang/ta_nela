<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengguna extends CI_Model {

	protected $table = 'tb_admin';

	public function fetchAll()
    {
        $this->db->select('*');
        return $this->db->get($this->table)->result();
    }

    public function get_where($data = '')
    {
        $this->db->where($data);
        return $this->db->get($this->table)->result();
    }

    public function insert($input = '')
    {
        $data = array(
            'nama'         => $input->nama,
            'alamat'         => $input->alamat,
            'username'         => $input->username,
            'no_telp'         => $input->no_telp,
            'level'         => $input->level,
            'password'         => md5($input->password),
            );
        $this->db->insert($this->table, $data);
    }

    public function update($input = '',$where)
    {
        $data = array();
        if($input->password == ''){
            $data = array(
            'nama'         => $input->nama,
            'alamat'         => $input->alamat,
            'username'         => $input->username,
            'no_telp'         => $input->no_telp,
            'level'         => $input->level,
            );
        }else{
        $data = array(
            'nama'         => $input->nama,
            'alamat'         => $input->alamat,
            'username'         => $input->username,
            'no_telp'         => $input->no_telp,
            'level'         => $input->level,
            'password'         => md5($input->password),

            );
        }
        $this->db->where($where);
        $this->db->update($this->table, $data);
    }

    public function delete($where)
    {
        $this->db->delete($this->table,$where);
    }

}