<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pelanggan extends CI_Model {

	protected $table = 'tb_pelanggan';

	public function fetchAll()
    {
        $this->db->select('*');
        return $this->db->get($this->table)->result();
    }

    public function get_laporan_pelanggan($start = null, $end = null){
        return $this->db->query("
            SELECT 
                tb_pelanggan.*, 
                (SELECT SUM(tb_penjualan.total) FROM tb_penjualan WHERE 
                    ".($start != null && $end != null?" tanggal_order >= '$start' AND tanggal_order <= '$end' AND ":" ")." id_pelanggan = tb_pelanggan.id) AS total,
                (SELECT COUNT(tb_penjualan.id) FROM tb_penjualan WHERE 
                    ".($start != null && $end != null?" tanggal_order >= '$start' AND tanggal_order <= '$end' AND ":" ")." id_pelanggan = tb_pelanggan.id) AS jml_transaksi
            FROM tb_pelanggan WHERE 1 
            ORDER BY total DESC 
        ")->result();
    }

    public function countAll()
    {
        $this->db->select('*');
        return $this->db->get($this->table)->num_rows();
    }

    public function get_where($data = '')
    {
        $this->db->where($data);
        return $this->db->get($this->table)->result();
    }

    public function insert($input = '')
    {
        $data = array(
            'kode'         => $input->kode,
            'nama'         => $input->nama,
            'no_telp'         => $input->no_telp,
            'alamat'         => $input->alamat,
            
            );
        $this->db->insert($this->table, $data);
    }

    public function update($input = '',$where)
    {
        
        $data = array(
        'kode'         => $input->kode,
        'nama'         => $input->nama,
        'no_telp'         => $input->no_telp,
        'alamat'         => $input->alamat,

        );
       
        $this->db->where($where);
        $this->db->update($this->table, $data);
    }

    public function delete($where)
    {
        $this->db->delete($this->table,$where);
    }

}