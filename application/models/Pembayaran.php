<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pembayaran extends CI_Model {

	protected $table = 'tb_pembayaran';

    public function removeDot($value='')
    {
        return str_replace('.', '', $value);
    }

	public function fetchAll()
    {
        $this->db->select('*');
        return $this->db->get($this->table)->result();
    }

    public function get_where($data = '')
    {
        $this->db->where($data);
        return $this->db->get($this->table)->result();
    }

    public function getPembayaran($faktur='')
    {
        $this->db->where(array('nomor_faktur'=>$faktur));
        return $this->db->get($this->table)->row();
    }

    public function getTotalPenjualan($request='')
    {
        $this->db->select_sum('total');
        if (@$request['tgl_start'] != '' && @$request['tgl_end'] != '') {
            $this->db->where('tanggal_order >=',$request['tgl_start']);
            $this->db->where('tanggal_order <=',$request['tgl_end']);
        }
        return $this->db->get('tb_penjualan')->row()->total;
    }

    public function getTotalPokok($request='')
    {
        $this->db->select_sum('total_pokok');
        if (@$request['tgl_start'] != '' && @$request['tgl_end'] != '') {
            $this->db->where('tgl_order >=',$request['tgl_start']);
            $this->db->where('tgl_order <=',$request['tgl_end']);
        }
        return $this->db->get('tb_detail_penjualan')->row()->total_pokok;
    }

    public function getTotalKeuntungan($request='')
    {
        $this->db->select_sum('total');
        if (@$request['tgl_start'] != '' && @$request['tgl_end'] != '') {
            $this->db->where('tanggal_order >=',$request['tgl_start']);
            $this->db->where('tanggal_order <=',$request['tgl_end']);
        }
        $penjualan = $this->db->get('tb_penjualan')->row()->total;

        $this->db->select_sum('total_pokok');
        if (@$request['tgl_start'] != '' && @$request['tgl_end'] != '') {
            $this->db->where('tgl_order >=',$request['tgl_start']);
            $this->db->where('tgl_order <=',$request['tgl_end']);
        }
        $pokok = $this->db->get('tb_detail_penjualan')->row()->total_pokok;

        return $penjualan-$pokok;
    }

    public function insert($input = '')
    {
        $data = array(
            'kode'         => $input->kode,
            'nomor_faktur'         => $input->faktur,
            'tanggal_pembayaran'         => $input->tgl,
            'sisa_pembayaran'         => $input->sisa,
            
            );
        $this->db->insert($this->table, $data);
    }

    public function update($input = '',$where)
    {
        
        $data = array(
            'kode'         => $input->kode,
            'nomor_faktur'         => $input->faktur,
            'tanggal_pembayaran'         => $input->tgl,
            'sisa_pembayaran'         => $input->sisa,

        );
       
        $this->db->where($where);
        $this->db->update($this->table, $data);
    }

    public function delete($where)
    {
        $this->db->delete($this->table,$where);
    }

}