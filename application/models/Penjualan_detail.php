<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Penjualan_detail extends CI_Model {

	protected $table = 'tb_detail_penjualan';

    public function removeDot($value='')
    {
        return str_replace('.', '', $value);
    }

	public function fetchAll()
    {
        $this->db->select('*');
        return $this->db->get($this->table)->result();
    }


    public function get_where($data = '')
    {
        $this->db->where($data);
        return $this->db->get($this->table)->result();
    }

    public function insert($input = '')
    {
        for ($i=0; $i < count($input->kode_barang); $i++) { 
            $data = array(
                'nomor_faktur'         => $input->nomor_faktur,
                'tgl_order'         => $input->tanggal_order,
                'id_produk'         => $input->kode_barang[$i],
                'jumlah'         => $input->jumlah[$i],
                'satuan'         => $input->satuan[$i],
                'harga_jual'         => $this->removeDot($input->harga_jual[$i]),
                'subtotal'         => $this->removeDot($input->sub_total[$i]),
                'total_pokok'         => $this->removeDot($input->harga_pokok[$i])*$this->removeDot($input->jumlah[$i]),
                'harga_pokok'         => $this->removeDot($input->harga_pokok[$i]),
                );
            $this->db->insert($this->table, $data);
        }
    }

    public function update($input = '')
    {
        $this->delete(array('nomor_faktur'=>$input->nomor_faktur));   
         for ($i=0; $i < count($input->kode_barang); $i++) { 
            $data = array(
                'nomor_faktur'         => $input->nomor_faktur,
                'tgl_order'         => $input->tanggal_order,
                'id_produk'         => $input->kode_barang[$i],
                'jumlah'         => $input->jumlah[$i],
                'satuan'         => $input->satuan[$i],
                'harga_jual'         => $this->removeDot($input->harga_jual[$i]),
                'subtotal'         => $this->removeDot($input->sub_total[$i]),
                'total_pokok'         => $this->removeDot($input->harga_pokok[$i])*$this->removeDot($input->jumlah[$i]),
                'harga_pokok'         => $this->removeDot($input->harga_pokok[$i]),
                );
            $this->db->insert($this->table, $data);
        }
    }

    public function delete($where)
    {
        $this->db->delete($this->table,$where);
    }

}