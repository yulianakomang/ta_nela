<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Penjualan extends CI_Model {

	protected $table = 'tb_penjualan';

    public function removeDot($value='')
    {
        return str_replace('.', '', $value);
    }

	public function fetchAll()
    {
        $this->db->select('*');
        return $this->db->get($this->table)->result();
    }
    public function countAll()
    {
        $this->db->select('*');
        return $this->db->get($this->table)->num_rows();
    }

    public function getCondition($request='')
    {
        if (@$request['tgl_start'] != '' && @$request['tgl_end'] != '') {
            $this->db->where('tanggal_order >=',$request['tgl_start']);
            $this->db->where('tanggal_order <=',$request['tgl_end']);
        }
        return $this->db->get($this->table)->result();
    }


    public function get_where($data = '')
    {
        $this->db->where($data);
        return $this->db->get($this->table)->result();
    }

    public function getPenjualan($id_pelanggan='',$request='')
    {
        $this->db->where(array('id_pelanggan'=> $id_pelanggan));
        if (@$request['tgl_start'] != '' && @$request['tgl_end'] != '') {
            $this->db->where('tanggal_order >=',$request['tgl_start']);
            $this->db->where('tanggal_order <=',$request['tgl_end']);
        }
        return $this->db->get($this->table)->num_rows();
    }
    public function getPenjualanTotal($id_pelanggan='',$request='')
    {
        $this->db->select_sum('total');
        $this->db->where(array('id_pelanggan'=> $id_pelanggan));
        if (@$request['tgl_start'] != '' && @$request['tgl_end'] != '') {
            $this->db->where('tanggal_order >=',$request['tgl_start']);
            $this->db->where('tanggal_order <=',$request['tgl_end']);
        }
        return $this->db->get($this->table)->result()[0];
    }

    public function update_status($input='')
    {
         $data = array(
            'status'   => 'Lunas',
        );
        $this->db->where(array('nomor_faktur'=> $input->faktur));
        $this->db->update($this->table, $data);
    }

    public function insert($input = '')
    {
        $data = array(
            'nomor_faktur'         => $input->nomor_faktur,
            'id_admin'         => $input->id_admin,
            'id_pelanggan'         => $input->id_pelanggan,
            'tanggal_order'         => $input->tanggal_order,
            'tanggal_terima'         => $input->tanggal_terima,
            'uang_muka'         => $this->removeDot($input->uang_muka),
            'total'         => $this->removeDot($input->total),
            'status'         => '',
            'sisa' => ($this->removeDot($input->total) - $this->removeDot($input->uang_muka)),
            );
        $this->db->insert($this->table, $data);
    }

    public function update($input = '',$where)
    {
        
        $data = array(
            'nomor_faktur'         => $input->nomor_faktur,
            'id_admin'         => $input->id_admin,
            'id_pelanggan'         => $input->id_pelanggan,
            'tanggal_order'         => $input->tanggal_order,
            'tanggal_terima'         => $input->tanggal_terima,
            'uang_muka'         => $this->removeDot($input->uang_muka),
            'total'         => $this->removeDot($input->total),
            'status'         => '',
            'sisa' => ($this->removeDot($input->total) - $this->removeDot($input->uang_muka)),
        );
       
        $this->db->where($where);
        $this->db->update($this->table, $data);
    }

    public function delete($where)
    {
        $this->db->delete($this->table,$where);
    }

}