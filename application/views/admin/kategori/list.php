<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <?php $this->view('admin/parts/upper') ?>
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Kategori Barang</h1>
        </div>
        
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-header">
            <div class="float-left">
              <button data-toggle="modal" data-target="#tambahData" class="btn btn-success">Tambah Kategori Barang</button>
            </div>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <table class="table table-bordered table-striped load_datatables">
              <thead>
                <tr>
                  <th>Nama Kategori</th>
                  <th>Deskripsi</th>
                  <th>Opsi</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach ($list as $item): ?>
                  
                <tr>
                  
                  <td><?= $item->nama_kategori ?></td>
                  <td><?= $item->deskripsi ?></td>
                  <td>
                    <a href="#" class="btn btn-sm btn-success edit_data" data-toggle="modal" data-target="#editData"
                          data-id="<?= $item->id ?>" 
                          data-nama="<?= $item->nama_kategori ?>" 
                          data-desc="<?= $item->deskripsi ?>" >Ubah</a>
                    <!-- <a href="<?= base_url('admin/kategori_hapus/'.$item->id) ?>" class="btn btn-sm btn-danger">Hapus</a> -->
                    
                  </td>
                </tr>
                <?php endforeach ?>
                
              </tfoot>
            </table>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
  <!-- /.content-wrapper -->


  <!-- Modal -->
<div class="modal fade" id="tambahData" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <form action="<?= base_url('admin/kategori_store') ?>" method="post">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel" style="text-align: center; margin:auto">Tambah Kategori Barang</h5>
        
      </div>
      <div class="modal-body">
        <div class="form-group">
            <label>Nama</label>
            <input type="text" name="nama_kategori" class="form-control" oninvalid="InvalidMsg(this);" oninput="InvalidMsg(this);" required>
          </div>
          <div class="form-group">
            <label>Deskripsi</label>
            <input type="text" name="deskripsi" class="form-control" oninvalid="InvalidMsg(this);" oninput="InvalidMsg(this);" required>
          </div>
      </div>
      <div class="modal-footer">
        <button class="btn btn-primary">Simpan</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal" aria-label="Close">Batal</button>
      </div>
      </form>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="editData" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <form action="<?= base_url('admin/kategori_update') ?>" method="post">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel" style="text-align: center; margin:auto">Ubah Kategori Barang</h5>
        
      </div>
      <div class="modal-body">
        <input type="hidden" name="id" id="form_id">
        <div class="form-group">
            <label>Nama</label>
            <input type="text" name="nama_kategori" id="form_nama" class="form-control" oninvalid="InvalidMsg(this);" oninput="InvalidMsg(this);" required>
          </div>
          <div class="form-group">
            <label>Deskripsi</label>
            <input type="text" name="deskripsi" id="form_deskripsi" class="form-control" oninvalid="InvalidMsg(this);" oninput="InvalidMsg(this);" required>
          </div>
      </div>
      <div class="modal-footer">
        <button class="btn btn-primary">Simpan</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal" aria-label="Close">Batal</button>
      </div>
      </form>
    </div>
  </div>
</div>

<script src="<?= base_url('asset/') ?>plugins/jquery/jquery.min.js"></script>
  <script src="<?= base_url('asset/') ?>sweetalert2/dist/sweetalert2.all.js"></script>
<script type="text/javascript">

    <?php 
    $notif = @$this->session->flashdata('sweetalert');
    $opsi = @$this->session->flashdata('opsi');
    $mode = 'Ditambah!';
    if($opsi == 'tambah'){ $mode = 'Ditambah!';}elseif($opsi =='ubah'){ $mode = 'Diubah!';}else{$mode='dihapus!';}
    if($notif !== NULL){ ?>
      Swal.fire(
  'Berhasil!',
  'Data berhasil <?= $mode ?>',
  '<?= $notif ?>'
);
    <?php } ?>
    $('.edit_data').click(function () {
     var 
     id = $(this).data('id'),
     nama = $(this).data('nama'),
     desc = $(this).data('desc');
     

      $('#form_id').val(id);
      $('#form_nama').val(nama);
      $('#form_deskripsi').val(desc);
    });
</script>
