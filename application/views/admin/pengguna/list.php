<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <?php $this->view('admin/parts/upper') ?>
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Data Pengguna</h1>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-12">

        <div class="card">
          <div class="card-header">
            <div class="float-left">
              <button data-toggle="modal" data-target="#tambahData" class="btn btn-success">Tambah Data Pengguna</button>
            </div>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <table class="table table-bordered table-striped load_datatables">
              <thead>
                <tr>
                  <th>Nama Pengguna</th>
                  <th>Alamat Pengguna</th>
                  <th>No Telp</th>
                  <th>Username</th>
                  <th>Opsi</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach ($list as $item): ?>

                  <tr>

                    <td><?= $item->nama ?></td>
                    <td><?= $item->alamat ?></td>
                    <td><?= $item->no_telp ?></td>
                    <td><?= $item->username ?></td>
                    <td>
                      <a href="#" class="btn btn-sm btn-success edit_data" data-toggle="modal" data-target="#editData"
                          data-id="<?= $item->id ?>" 
                          data-nama="<?= $item->nama ?>" 
                          data-alamat="<?= $item->alamat ?>" 
                          data-no_telp="<?= $item->no_telp ?>" 
                          data-level="<?= $item->level ?>" 
                          data-username="<?= $item->username ?>" >Ubah</a>
                      <!-- <a href="<?= base_url('admin/pengguna_hapus/'.$item->id) ?>" class="btn btn-sm btn-danger">Hapus</a>  -->
                      
                    </td>
                  </tr>
                <?php endforeach ?>
                
              </tfoot>
            </table>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!-- Modal -->
<div class="modal fade" id="tambahData" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <form action="<?= base_url('admin/pengguna_store') ?>" method="post">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel" style="text-align: center; margin:auto">Tambah Data Pengguna</h5>
         
        </div>
        <div class="modal-body">
          <div class="form-group">
            <label>Nama pengguna</label>
            <input type="text" name="nama" class="form-control" oninvalid="InvalidMsg(this);" oninput="InvalidMsg(this);" required>
          </div>
          <div class="form-group">
            <label>Alamat pengguna</label>
            <input type="text" name="alamat" class="form-control" oninvalid="InvalidMsg(this);" oninput="InvalidMsg(this);" required>
          </div>
          <div class="form-group">
            <label>No Telp</label>
            <input type="text" name="no_telp" class="form-control" oninvalid="InvalidMsg(this);" oninput="InvalidMsg(this);" required>
          </div>
          <div class="form-group">
            <label>Username</label>
            <input type="text" name="username" class="form-control" oninvalid="InvalidMsg(this);" oninput="InvalidMsg(this);" required>
          </div>
          <div class="form-group">
            <label>Jabatan</label>
            <select name="level" id="" class="form-control">
              <option value="0">Admin Toko</option>
              <option value="1">Pemilik Toko</option>
            </select>
          </div>
          <div class="form-group">
            <label>Password </label>
            <input type="text" name="password" class="form-control" oninvalid="InvalidMsg(this);" oninput="InvalidMsg(this);" required>
          </div>
        </div>
        <div class="modal-footer">
          <button class="btn btn-primary">Simpan</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal" aria-label="Close">Batal</button>
        </div>
      </form>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="editData" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <form action="<?= base_url('admin/pengguna_update') ?>" method="post">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel" style="text-align: center; margin:auto">Ubah Data Penggun </h5>
          
        </div>
        <div class="modal-body">
          <input type="hidden" name="id" id="form_id">
          <div class="form-group">
            <label>Nama Pengguna</label>
            <input type="text" name="nama" id="form_nama" class="form-control" oninvalid="InvalidMsg(this);" oninput="InvalidMsg(this);" required>
          </div>
          <div class="form-group">
            <label>Alamat Pengguna</label>
            <input type="text" name="alamat" id="form_alamat" class="form-control" oninvalid="InvalidMsg(this);" oninput="InvalidMsg(this);" required>
          </div>
          <div class="form-group">
            <label>No Telp</label>
            <input type="text" name="no_telp" id="form_no_telp" class="form-control" oninvalid="InvalidMsg(this);" oninput="InvalidMsg(this);" required>
          </div>
          <div class="form-group">
            <label>Username</label>
            <input type="text" name="username" id="form_username" class="form-control" oninvalid="InvalidMsg(this);" oninput="InvalidMsg(this);" required>
          </div>
          <div class="form-group">
            <label>Roles</label>
            <select name="level" id="form_level" class="form-control">
              <option value="0">Admin Toko</option>
              <option value="1">Pemilik Toko</option>
            </select>
          </div>
          <div class="form-group">
            <label>Password ( Isi untuk Mengganti yang lama. )</label>
            <input type="text" name="password" id="form_password" class="form-control" oninvalid="InvalidMsg(this);" oninput="InvalidMsg(this);" required>
          </div>
        </div>
        <div class="modal-footer">
          <button class="btn btn-primary">Simpan</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal" aria-label="Close">Batal</button>
        </div>
      </form>
    </div>
  </div>
</div>

<script src="<?= base_url('asset/') ?>plugins/jquery/jquery.min.js"></script>
  <script src="<?= base_url('asset/') ?>sweetalert2/dist/sweetalert2.all.js"></script>
<script type="text/javascript">

    <?php 
    $notif = @$this->session->flashdata('sweetalert');
    $opsi = @$this->session->flashdata('opsi');
    $mode = 'Ditambah!';
    if($opsi == 'tambah'){ $mode = 'Ditambah!';}elseif($opsi =='ubah'){ $mode = 'Diubah!';}else{$mode='dihapus!';}
    if($notif !== NULL){ ?>
      Swal.fire(
  'Berhasil!',
  'Data Pengguna Berhasil <?= $mode ?>',
  '<?= $notif ?>'
);
    <?php } ?>

    $('.edit_data').click(function () {
     var 
     id = $(this).data('id'),
     nama = $(this).data('nama'),
     alamat = $(this).data('alamat'),
     no_telp = $(this).data('no_telp'),
     level = $(this).data('level'),
     username = $(this).data('username');
     

      $('#form_id').val(id);
      $('#form_nama').val(nama);
      $('#form_alamat').val(alamat);
      $('#form_no_telp').val(no_telp);
      $('#form_level').val(level);
      $('#form_username').val(username);
    });
</script>
