<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <?php $this->view('admin/parts/upper') ?>
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>DataPengguna</h1>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-12">


        <div class="card">
          <!-- /.card-header -->
          <div class="card-body">
            <?php if ($form_mode == 'add'): ?>
            <form action="<?= base_url('admin/pengguna_store') ?>" method="post">
              <?php else: ?>
            <form action="<?= base_url('admin/pengguna_update') ?>" method="post">
              <input type="hidden" name="id" value="<?= $this->uri->segment(3); ?>">
            <?php endif ?>
              <div class="form-group">
                <label>Nama</label>
                <input type="text" name="nama" class="form-control" value="<?= @$hasil[0]->nama ?>">
              </div>
              <div class="form-group">
                <label>Alamat</label>
                <input type="text" name="alamat" class="form-control" value="<?= @$hasil[0]->alamat ?>">
              </div>
              <div class="form-group">
                <label>Username</label>
                <input type="text" name="username" class="form-control" value="<?= @$hasil[0]->username ?>">
              </div>
              <div class="form-group">
                <label>Password <?php if ($form_mode == 'edit'): ?>
                  ( Isi untuk Mengganti yang lama. )
                <?php endif ?></label>
                <input type="text" name="password" class="form-control">
              </div>
              
              <div class="form-group">
                <div class="float-right">
                  <button class="btn btn-success">Simpan</button>
                  <a href="#" onclick="return window.history.back();" class="btn btn-danger">Batal</a>
                </div>
              </div>
            </form>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
  <!-- /.content-wrapper -->