<!DOCTYPE html>
<html>
<head>
  <title>Penjualan</title>
  <!-- Theme style -->
  <link rel="stylesheet" href="<?= base_url('asset/') ?>dist/css/adminlte.min.css">
</head>
<body>
  
  <div class="row pt-2" style="margin-top: 100px">
    <div class="col-sm-4">
      <div class="float-right">
        <img src="<?= base_url('asset/logo.png') ?>" width="120px">
      </div>
    </div>
    <div class="col-sm-5 text-center">
      <div class="col-sm-12 text-center"><h1>Laporan Pelanggan</h1>
      <h4>STUDIO GRAFIZ CARD and Souvenir</h4>
      <?php if (@$_GET['tgl_start']): ?>
      <h6> Periode : <?= date('d-m-Y',strtotime(@$_GET['tgl_start'])) ?> s/d  <?= date('d-m-Y',strtotime(@$_GET['tgl_end'])) ?></h6>
        <?php else: ?>
          <h6>Semua Periode</h6>
      <?php endif ?>
    </div>
  </div>

<table class="table table-bordered table-striped">
  <thead>
    <tr>
      <th>Kode Pelanggan </th>
      <th>Nama Pelanggan </th>
      <th>No Telpon </th>
      <th>Alamat Pelanggan</th>
      <th>Jumlah Transaksi</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($list as $item): ?>

      <tr>

        <td><?= $item->kode ?></td>
        <td><?= $item->nama ?></td>
        <td><?= $item->no_telp ?></td>
        <td><?= $item->alamat ?></td>
        <td>Rp. <?= number_format($func->getPenjualanTotal($item->id,@$_GET)->total,'0','','.') ?></td>
       
      </tr>
    <?php endforeach ?>
    
  </tfoot>
</table>

<script type="text/javascript">
  window.print();
  window.onfocus=function(){ window.close();}
</script>
</body>
</html>