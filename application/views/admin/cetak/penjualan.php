<!DOCTYPE html>
<html>
<head>
  <title>Penjualan</title>
  <!-- Theme style -->
  <link rel="stylesheet" href="<?= base_url('asset/') ?>dist/css/adminlte.min.css">
</head>
<body>
<div class="row pt-2" style="margin-top: 100px">
    <div class="col-sm-4">
      <div class="float-right">
        <img src="<?= base_url('asset/logo.png') ?>" width="120px">
      </div>
    </div>
    <div class="col-sm-5 text-center">
      <div class="col-sm-12 text-center"><h1>Laporan Penjualan</h1>
      <h4>STUDIO GRAFIZ CARD and Souvenir</h4>
      <?php if (@$_GET['tgl_start']): ?>
      <h6> Periode : <?= date('d-m-Y',strtotime(@$_GET['tgl_start'])) ?> s/d  <?= date('d-m-Y',strtotime(@$_GET['tgl_end'])) ?></h6>
        <?php else: ?>
          <h6>Semua Periode</h6>
      <?php endif ?>
    </div>
  </div>
<table class="table table-bordered table-striped">
  <thead>
    <tr>
      <th>No.</th>
      <th>TGL Order </th>
      <th>No Faktur </th>                  
      <th>Uang Muka </th>
      <th>Sisa Pembayaran  </th>
      <th>Nama Pelanggan</th>
      <th>Total Harga</th>
    </tr>
  </thead>
  <tbody>
    <?php 
    foreach ($list as $key => $item): ?>
      <tr>
        <td><?=($key+1)?></td>
        <td><?= date('d-m-Y',strtotime($item->tanggal_order ))?></td>
        <td><?= $item->nomor_faktur ?></td> 
        <td>Rp. <?= number_format($item->uang_muka,0,'','.') ?></td> 
        <td>Rp. <?= number_format(@$func->getPembayaran($item->nomor_faktur)->sisa_pembayaran,0,'','.') ?></td>                  
        <td><?php foreach ($pelanggan_list as $plg): ?>
          <?php if ($plg->id == $item->id_pelanggan): ?>
            <?= $plg->nama ?>
          <?php endif ?>
        <?php endforeach ?></td>
        <td>Rp. <?= number_format($item->total,0,'','.') ?></td>
        
      </tr>
    <?php endforeach ?>
    
  </tfoot>
</table>

<script type="text/javascript">
  window.print();
  window.onfocus=function(){ window.close();}
</script>
</body>
</html>