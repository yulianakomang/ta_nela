<div class="user-panel mt-3 pb-3 mb-3 d-flex">
        
        <div class="info text-center w-100">
          <a href="#" class="d-block"><i class="fa fa-user"></i> &nbsp; <?= ($this->session->userdata('level')== '1')?'Pemilik Toko':'Admin Toko'; ?> <br> <?= $this->session->userdata('nama') ?></a>
        </div>
      </div>

<!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item">
            <a href="<?= base_url('admin') ?>" class="nav-link">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>Menu Utama</p>
            </a>
          </li>
          <?php if ($this->session->userdata('level') === '0' ): ?>
         <li class="nav-item">
                <a href="<?= base_url('admin/pelanggan') ?>" class="nav-link">
                  <i class="far fa-user nav-icon"></i>
                  <p>Data Pelanggan</p>
                </a>
              </li> 
          <li class="nav-item">
            <a href="<?= base_url('admin/penjualan') ?>" class="nav-link">
              <i class="nav-icon fas fa-chart-pie"></i>
              <p>Data Penjualan</p>
            </a>
          </li>
          
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-copy"></i>
              <p>
                Data Barang
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?= base_url('admin/kategori') ?>" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Kategori Barang</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?= base_url('admin/produk') ?>" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Data Produk</p>
                </a>
              </li>
              
            </ul>
            <?php endif ?>
            <?php if ($this->session->userdata('level') === '1' ): ?>
              
            <li class="nav-item">
                <a href="<?= base_url('admin/pengguna') ?>" class="nav-link">
                  <i class="far fa-user nav-icon"></i>
                  <p>Data Pengguna</p>
                </a>
          </li>
          <li class="nav-header">LAPORAN</li>
          <li class="nav-item">
            <a href="<?= base_url('admin/laporan_pelanggan') ?>" class="nav-link">
              <i class="nav-icon fas fa-book"></i>
              <p>Laporan Pelanggan</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?= base_url('admin/laporan_penjualan') ?>" class="nav-link">
              <i class="nav-icon fas fa-book"></i>
              <p>Laporan Penjualan</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?= base_url('admin/laporan_data_barang') ?>" class="nav-link">
              <i class="nav-icon fas fa-book"></i>
              <p>Laporan Barang</p>
            </a>
          </li>
            <?php endif ?>

        </ul>
      </nav>