<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <?php $this->view('admin/parts/upper') ?>
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Data Penjualan</h1>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-12">


        <div class="card">
          <div class="modal-header">
           <?php if ($form_mode == 'add'): ?>
            <h5 class="modal-title" id="exampleModalLabel" style="text-align: center; margin:auto">Tambah Data Penjualan</h5>
              <?php else: ?>
            <h5 class="modal-title" id="exampleModalLabel" style="text-align: center; margin:auto">Ubah Data Penjualan</h5>
            <?php endif ?>
      </div>
          <div class="card-body">
            <?php if ($form_mode == 'add'): ?>
              <form action="<?= base_url('admin/penjualan_store') ?>" method="post"  id="myForm">
                <?php else: ?>
                  <form action="<?= base_url('admin/penjualan_update') ?>" method="post" id="myForm">
                    <input type="hidden" name="id" value="<?= $this->uri->segment(3); ?>">
                  <?php endif ?>
                  <input type="hidden" name="id_admin" value="<?= $this->session->userdata('id') ?>">
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Nomor Faktur</label>
                        <input type="text" name="nomor_faktur" class="form-control required" value="<?= (@$hasil[0]->nomor_faktur == NULL)? 'PJ-'.date('Ymdhis'): @$hasil[0]->nomor_faktur; ?>" readonly>
                      </div>
                      <div class="form-group">
                        <label>Pelanggan</label>
                        <select name="id_pelanggan" class="form-control">
                          <?php foreach ($pelanggan_list as $pelanggan): ?>
                            <option <?php if ($pelanggan->id == @$hasil[0]->id_pelanggan): ?>

                            <?php endif ?> value="<?= $pelanggan->id ?>">[ <?= $pelanggan->kode ?> ] <?= $pelanggan->nama ?></option>
                          <?php endforeach ?>
                        </select>
                      </div>


                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Tanggal Order</label>
                        <input type="date" name="tanggal_order" class="form-control" value="<?= (@$hasil[0]->tanggal_order === NULL)? date('Y-m-d'):@$hasil[0]->tanggal_order ; ?>">
                      </div>
                      <div class="form-group">
                        <label>Tanggal Terima</label>
                        <input type="date" name="tanggal_terima" class="form-control" value="<?= (@$hasil[0]->tanggal_terima === NULL)? date('Y-m-d'):$hasil[0]->tanggal_terima ; ?>">
                      </div>
                      <div class="form-group">
                        <button type="button" class="btn btn-success trigger_tambah_produk">Tambah Transaksi</button></div>
                        
                      </div>

                    </div>
                  

                  <div class="list_load_produk">



                        <?php if ($form_mode == 'edit'): ?>
                          <?php $current_cat = ''; ?>
                        <?php $index = 1; foreach ($detail_list as $key => $detil): ?>
                    <div class="card item-barang">
                      <div class="container">
                        <div class="row">
                          <div class="col-md-2">
                            <div class="form-group">
                              <?php if ($index == '1'): ?>
                                <label>
                                  Pilih Barang
                                </label>
                                <?php else: ?>
                                  <br>
                              <?php endif ?>
                              <select name="kode_barang[]" id="" class="form-control ambilproduk-<?= $index ?>" data-order="<?= $index ?>">
                                <option value="">--Pilih--</option>
                                <?php foreach ($produk_list as $item): ?>
                                  <option 
                                  <?php if ($detil->id_produk == $item->id): ?>
                                    selected
                                    <?php foreach ($kategori_list as $cat): ?>
                                   <?php if ($cat->id == $item->kategori_id): ?>
                                     <?php $current_cat = $cat->nama_kategori ?>
                                   <?php endif ?>
                                 <?php endforeach ?>
                                  <?php endif ?>

                                  data-id="<?= $item->id ?>" 
                                  data-kode_produk="<?= $item->kode_produk ?>" 
                                  data-satuan="<?= $item->satuan ?>" 
                                  data-harga="<?= number_format($item->harga,'0','','.') ?>" 
                                  data-harga_jual="<?= number_format($item->harga_jual,'0','','.') ?>" 
                                  data-kategori="<?php foreach ($kategori_list as $cat): ?>
                                   <?php if ($cat->id == $item->kategori_id): ?>
                                     <?= $cat->nama_kategori ?>
                                   <?php endif ?>
                                 <?php endforeach ?>"
                                 value="<?= $item->id ?>"> [ <?= $item->kode_produk ?> ] <?= $item->nama ?></option>
                                <?php endforeach ?>
                              </select>
                            </div>
                          </div>
                         <div class="col-md-2">
                            <div class="form-group">
                              <?php if ($index == '1'): ?>
                              <label>
                                Kategori
                              </label>
                              <?php else: ?>
                                  <br>
                            <?php endif ?>
                              <input type="text" id="kategori-<?= $index ?>" readonly class="form-control" data-order="<?= $index ?>" value="<?= $current_cat ?>">
                            </div>
                          </div>
                          <div class="col-md-2">
                            <div class="form-group">
                              <?php if ($index == '1'): ?>
                              <label>
                                Satuan
                              </label>
                              <?php else: ?>
                                  <br>
                            <?php endif ?>
                              <input type="text" name="satuan[]" id="satuan-<?= $index ?>" readonly class="form-control" data-order="<?= $index ?>" value="<?= $detil->satuan ?>">
                            </div>
                          </div>
                          <div class="col-md-2">
                            <div class="form-group">
                              <?php if ($index == '1'): ?>
                              <label>
                                Harga Jual
                              </label>
                              <?php else: ?>
                                  <br>
                            <?php endif ?>
                              <input type="text" name="harga_jual[]" id="harga_jual-<?= $index ?>" readonly class="form-control price-format" data-order="<?= $index ?>" value="<?= number_format($detil->harga_jual,'0','','.') ?>">
                              <input type="hidden" name="harga_pokok[]" id="harga_pokok-<?= $index ?>" readonly class="form-control" data-order="<?= $index ?>" value="<?= $detil->harga_pokok ?>">
                            </div>
                          </div>
                          <div class="col-md-2">
                            <div class="form-group">
                              <?php if ($index == '1'): ?>
                              <label>
                                Jumlah Pesanan
                              </label>
                              <?php else: ?>
                                  <br>
                            <?php endif ?>
                              <input type="number" min="1" name="jumlah[]" id="jumlah-<?= $index ?>" class="form-control" data-order="<?= $index ?>" value="<?= $detil->jumlah ?>">
                            </div>
                          </div>
                          <div class="col-md-2">
                            <div class="form-group">
                              <?php if ($index == '1'): ?>
                              <label>
                                Sub Total
                              </label>
                              <?php else: ?>
                                  <br>
                            <?php endif ?>
                                <span style="position: absolute;right: 8px;top: 2px;cursor: pointer"><i class="fa fa-times hapus_item"></i></span>
                              <input type="text" name="sub_total[]" id="sub_total-<?= $index ?>" readonly class="form-control" data-order="<?= $index ?>" value="<?= number_format($detil->subtotal,'0','','.') ?>">
                            </div>

                          </div>
                        </div>
                      </div>
                    </div>

                        <?php $index++; endforeach ?>
                        <?php else: ?>
                          <div class="card item-barang">
                      <div class="container">
                        <div class="row">
                          <div class="col-md-2">
                            <div class="form-group">
                              <label>
                                Pilih Barang
                              </label>
                              <select name="kode_barang[]" id="" class="form-control ambilproduk-1" data-order="1">
                                <option value="">--Pilih--</option>
                                <?php foreach ($produk_list as $item): ?>
                                  <option 
                                  data-id="<?= $item->id ?>" 
                                  data-kode_produk="<?= $item->kode_produk ?>" 
                                  data-satuan="<?= $item->satuan ?>" 
                                  data-harga="<?= number_format($item->harga,'0','','.') ?>" 
                                  data-harga_jual="<?= number_format($item->harga_jual,'0','','.') ?>" 
                                  data-kategori="<?php foreach ($kategori_list as $cat): ?><?php if ($cat->id == $item->kategori_id): ?><?= $cat->nama_kategori ?> <?php endif ?><?php endforeach ?>"
                                 value="<?= $item->id ?>">[ <?= $item->kode_produk ?> ] <?= $item->nama ?></option>
                                <?php endforeach ?>
                              </select>
                            </div>
                          </div>
                         <div class="col-md-2">
                            <div class="form-group">
                              <label>
                                Kategori
                              </label>
                              <input type="text" name="kategori[]" id="kategori-1" readonly class="form-control" data-order="1">
                            </div>
                          </div>
                          <div class="col-md-2">
                            <div class="form-group">
                              <label>
                                Satuan
                              </label>
                              <input type="text" name="satuan[]" id="satuan-1" readonly class="form-control" data-order="1">
                            </div>
                          </div>
                          <div class="col-md-2">
                            <div class="form-group">
                              <label>
                                Harga Jual
                              </label>
                              <input type="text" name="harga_jual[]" id="harga_jual-1" readonly class="form-control price-format" data-order="1">
                              <input type="hidden" name="harga_pokok[]" id="harga_pokok-1" readonly class="form-control" data-order="1">
                            </div>
                          </div>
                          <div class="col-md-2">
                            <div class="form-group">
                              <label>
                                Jumlah Pesanan
                              </label>
                              <input type="number" min="1" value="1" name="jumlah[]" id="jumlah-1" class="form-control" data-order="1">
                            </div>
                          </div>
                          <div class="col-md-2">
                            <div class="form-group">
                              <label>
                                Sub Total
                              </label>
                              <input type="text" name="sub_total[]" id="sub_total-1" readonly class="form-control price-format" data-order="1">
                            </div>
                          </div>
                        </div>
                      </div>

                    </div>
                  </div>
                        <?php endif ?>

                        



                  </div>

                  <div class="row">
                    <div class="col-md-4 offset-8">
                      <div class="text-right">
                        <button class="btn btn-success" id="kalkulasi"> <i class="fa fa-calendar"></i> Hitung</button>
                      </div>

                      <div class="form-group">
                        <label>Total</label>
                        <input type="text" name="total" id="total" class="form-control price-format" readonly value="<?= (@$hasil[0]->total == NULL)?'0':number_format(@$hasil[0]->total,'0','','.'); ?>">
                      </div>
                      <div class="form-group">
                        <label>Uang Muka</label>
                        <input type="text" name="uang_muka" id="uang_muka" class="form-control price-format" value="<?= (@$hasil[0]->uang_muka == NULL)?'0':number_format(@$hasil[0]->uang_muka,'0','','.'); ?>" min="0">
                      </div>
                    </div>
                  </div>


                  <div class="form-group">
                    <div class="float-right">
                      <button class="btn btn-success" id="save">Simpan</button>
                      <a href="#" onclick="return window.history.back();" class="btn btn-danger">Batal</a>
                    </div>
                  </div>
                </form>
              </div>
              
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </section>
      <!-- /.content -->
    </div>
  <!-- /.content-wrapper -->

<script src="<?= base_url('asset/') ?>plugins/jquery/jquery.min.js"></script>
<script src="<?= base_url('asset/') ?>sweetalert2/dist/sweetalert2.all.js"></script>
<script type="text/javascript">
  <?php if ($form_mode == 'edit'): ?>
  var jum_list = <?= $index ?>;
  <?php else: ?>
  var jum_list = 1;
<?php endif ?>
  var total = 0;
    $('.trigger_tambah_produk').click(function(){
      jum_list+=1;
      var html = '<div class="card item-barang"> <div class="container"> <div class="row"> <div class="col-md-2"> <div class="form-group"><br/><select name="kode_barang[]" id="" class="form-control ambilproduk-'+jum_list+'" data-order="'+jum_list+'"> <option value="">--Pilih--</option> <?php foreach ($produk_list as $item): ?> <option data-id="<?= $item->id ?>" data-kode_produk="<?= $item->kode_produk ?>" data-satuan="<?= $item->satuan ?>" data-harga="<?= number_format($item->harga,'0','','.') ?>" data-harga_jual="<?= number_format($item->harga_jual,'0','','.') ?>" data-kategori="<?php foreach ($kategori_list as $cat): ?><?php if ($cat->id == $item->kategori_id): ?><?= $cat->nama_kategori ?> <?php endif ?><?php endforeach ?>" value="<?= $item->id ?>">[ <?= $item->kode_produk ?> ] <?= $item->nama ?></option> <?php endforeach ?> </select> </div> </div> <div class="col-md-2"> <div class="form-group"><br/><input type="text" name="kategori[]" id="kategori-'+jum_list+'" readonly class="form-control" data-order="'+jum_list+'"> </div> </div> <div class="col-md-2"> <div class="form-group"><br/><input type="text" name="satuan[]" id="satuan-'+jum_list+'" readonly class="form-control" data-order="'+jum_list+'"> </div> </div> <div class="col-md-2"> <div class="form-group"><br/><input type="text" name="harga_jual[]" id="harga_jual-'+jum_list+'" readonly class="form-control price-format" data-order="'+jum_list+'"> <input type="hidden" name="harga_pokok[]" id="harga_pokok-'+jum_list+'" readonly class="form-control" data-order="'+jum_list+'"> </div> </div> <div class="col-md-2"> <div class="form-group"><br/><input type="number" min="1" value="1" name="jumlah[]" id="jumlah-'+jum_list+'" class="form-control" data-order="'+jum_list+'"> </div> </div> <div class="col-md-2"> <div class="form-group"><br/><span style="position: absolute;right: 8px;top: 2px;cursor: pointer"><i class="fa fa-times hapus_item"></i></span><input type="text" name="sub_total[]" id="sub_total-'+jum_list+'" readonly class="form-control price-format" data-order="'+jum_list+'"> </div> </div> </div> </div> </div> </div>';
      $('.list_load_produk').append(html);
    });
    $(document).on('change','[class*=ambilproduk-]',function(){
      var value = $(this).val();
      var satuan = $(this).find(':selected').data('satuan');
      var harga_jual = $(this).find(':selected').data('harga_jual');
      var harga = $(this).find(':selected').data('harga');
      var kategori = $(this).find(':selected').data('kategori');
      var dataOrder = $(this).data('order');
      var jumlah = $('#jumlah-'+dataOrder).val();
      
      $('#harga_jual-'+dataOrder).val(harga_jual);
      $('#harga_pokok-'+dataOrder).val(harga);
      $('#satuan-'+dataOrder).val(satuan);
      $('#kategori-'+dataOrder).val(kategori);

      var subtotal = parseInt(harga_jual.replace(/\./g,'')) * parseInt(jumlah.replace(/\./g,''));

      $('#sub_total-'+dataOrder).val(jsformatnumber(subtotal));

      maskformat();

    });
    $(document).on('change','[id*=jumlah-]',function(){
      var dataOrder = $(this).data('order');
      var harga = $('#harga_jual-'+dataOrder).val();
      var jumlah = $(this).val();
      var subtotal = parseInt(harga.replace(/\./g,'')) * parseInt(jumlah.replace(/\./g,''));
      $('#sub_total-'+dataOrder).val(jsformatnumber(subtotal));
     maskformat();
    });

    $('#kalkulasi').click(function(ev){
      ev.preventDefault();
      kalkulasi();
    });

    function kalkulasi() {
      var total = 0;
      $('[id*=sub_total-]').each(function(i,obj){
        var akg = obj.value;
        total += parseInt(akg.replace(/\./g,''));
      });
      $('#total').val(jsformatnumber(total));
      maskformat();
    }

    $('#save').click(function(ev){
      ev.preventDefault();
      //kalkulasi
      kalkulasi();
      // valid
      var  total = $('#total').val();
      if(total === null || total === '' || total === 0){
        Swal.fire(
          'Lengkapi data Penjualan!',
          'Pilih Barang dan Jumlah yang tepat!',
          'question'
        );
        return;
      }
      $('#myForm').submit();
    });

    $(document).on('click','.hapus_item',function(){
      $(this).parents('.item-barang').remove();
    });


</script>