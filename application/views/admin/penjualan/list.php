<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <?php $this->view('admin/parts/upper') ?>
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Data Penjualan</h1>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-12">

        <div class="card">
          <div class="card-header">
            <div class="float-left">
              <a href="<?= base_url('admin/penjualan_tambah') ?>" class="btn btn-success">Tambah Penjualan</a>
            </div>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <table class="table table-bordered table-striped load_datatables">
              <thead>
                <tr>
                  <th>No Faktur </th>                  
                  <th>Pelanggan </th>
                  <th>TGL Order </th>
                  <th>TGL Terima</th>
                  <th>Uang Muka</th>
                  <th>Total</th>
                  <th>Keterangan</th>
                  <th>Opsi</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach ($list as $item): ?>

                  <tr>

                    <td><a href="<?= base_url('admin/detail_pembayaran/'.$item->nomor_faktur) ?>" class="btn btn-success"><?= $item->nomor_faktur ?></a></td>                    
                    <td><?php foreach ($pelanggan_list as $plg): ?>
                      <?php if ($plg->id == $item->id_pelanggan): ?>
                        <?= $plg->nama ?>
                      <?php endif ?>
                    <?php endforeach ?></td>
                    <td><?= date('d-m-Y',strtotime($item->tanggal_order ))?></td>
                    <td><?= date('d-m-Y',strtotime($item->tanggal_terima)) ?></td>
                    <td>Rp. <?= number_format($item->uang_muka,0,'','.') ?></td>
                    <td>Rp. <?= number_format($item->total,0,'','.') ?></td>
                    <td><?php if ($item->status != 'Lunas'): ?>
                        Belum Lunas
                      <?php else: ?>
                      Lunas
                    <?php endif ?>
                      </td>
                    <td>
                      <?php if ($item->status != 'Lunas'): ?>
                      <a href="<?= base_url('admin/penjualan_ubah/'.$item->id) ?>" class="btn btn-sm btn-success">Ubah</a>
                      <button data-toggle="modal" data-target="#Pembayaran" class="btn btn-sm btn-success tambah_pembayaran" data-total="<?= $item->total ?>" data-uang_muka="<?= $item->uang_muka ?>" data-sisa="<?= $item->sisa ?>" data-faktur="<?= $item->nomor_faktur ?>">Pembayaran</button><?php endif ?>
                       <a href="<?= base_url('admin/penjualan_hapus/'.$item->id.'/'.$item->nomor_faktur) ?>" class="btn btn-sm btn-danger">Hapus</a> 
                    </td>
                  </tr>
                <?php endforeach ?>
                
              </tfoot>
            </table>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!-- Modal -->
<div class="modal fade" id="Pembayaran" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <form action="<?= base_url('admin/pembayaran_store') ?>" method="post">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel" style="text-align: center; margin:auto">Tambah Pembayaran</h5>
          
        </div>
        <div class="modal-body">
          <input type="hidden" name="faktur" id="form_faktur">
          <div class="form-group">
            <label>Kode Pembayaran</label>
            <input type="text" name="kode" id="form_kode" class="form-control" value="<?= 'PBY-'.date('Ymdhis') ?>" readonly>
          </div>
          <div class="form-group">
            <label>Tgl Pembayaran</label>
            <input type="date" name="tgl" id="form_pembayaran" class="form-control" value="<?= date('Y-m-d') ?>" readonly>
          </div>
          <div class="form-group">
            <label>Total Harga</label>
            <input type="text" name="total" id="form_total" class="form-control" readonly>
          </div>
          <div class="form-group">
            <label>Uang Muka </label>
            <input type="text" name="uang_muka" id="form_uang_muka" class="form-control" readonly>
          </div>
          <div class="form-group">
            <label>Sisa Pembayaran</label>
            <input type="text" name="sisa" id="form_sisa" class="form-control" readonly>
          </div>
        </div>
        <div class="modal-footer">
          <button class="btn btn-primary">Simpan</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal" aria-label="Close">Batal</button>
        </div>
      </form>
    </div>
  </div>
</div>

<script src="<?= base_url('asset/') ?>plugins/jquery/jquery.min.js"></script>
  <script src="<?= base_url('asset/') ?>sweetalert2/dist/sweetalert2.all.js"></script>
<script type="text/javascript">

    <?php 
    $notif = @$this->session->flashdata('sweetalert');
    $opsi = @$this->session->flashdata('opsi');
    $mode = 'Ditambah!';
    if($opsi == 'tambah'){ $mode = 'Ditambah!';}elseif($opsi =='ubah'){ $mode = 'Diubah!';}else{$mode='dihapus!';}
    if($notif !== NULL){ ?>
      Swal.fire(
  'Berhasil!',
  'Data Penjualan berhasil <?= $mode ?>',
  '<?= $notif ?>'
);
    <?php } ?>
    <?php if ($opsi == 'tambah-pembayaran') { ?>
      Swal.fire(
  'Berhasil!',
  'Data Pembayaran berhasil ditambah!',
  'success'
);
    <?php } ?>
    $('.tambah_pembayaran').click(function () {
     var 
     faktur = $(this).data('faktur'),
     total = $(this).data('total'),
     uang_muka = $(this).data('uang_muka'),
     sisa = $(this).data('sisa');
     

      $('#form_faktur').val(faktur);
      $('#form_total').val(total);
      $('#form_uang_muka').val(uang_muka);
      $('#form_sisa').val(sisa);
    });
</script>
