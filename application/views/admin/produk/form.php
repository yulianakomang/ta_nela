<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <?php $this->view('admin/parts/upper') ?>
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Data Produk</h1>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-12">


        <div class="card">
          <!-- /.card-header -->
          <div class="card-body">
            <?php if ($form_mode == 'add'): ?>
            <form action="<?= base_url('admin/produk_store') ?>" method="post">
              <?php else: ?>
            <form action="<?= base_url('admin/produk_update') ?>" method="post">
              <input type="hidden" name="id" value="<?= $this->uri->segment(3); ?>">
            <?php endif ?>
            <div class="form-group">
                <label>Kode Produk</label>
                <input type="text" name="kode_produk" class="form-control" value="<?= @$hasil[0]->kode_produk ?>">
              </div>
              <div class="form-group">
                <label>Nama</label>
                <input type="text" name="nama" class="form-control" value="<?= @$hasil[0]->nama ?>">
              </div>
               <div class="form-group">
                <label>Kategori</label>
                <select name="kategori_id" class="form-control">
                  <?php foreach ($kategori_list as $cat): ?>
                  <option <?php if ($cat->id == @$hasil[0]->kategori_id): ?>
                  <?php endif ?> value="<?= $cat->id ?>"><?= $cat->nama_kategori ?></option>
                  <?php endforeach ?>
                </select>
              </div>
              <div class="form-group">
                <label>Harga pokok</label>
                <input type="text" name="harga" class="form-control" value="<?= @$hasil[0]->harga ?>">
              </div>
              <div class="form-group">
                <label>Harga Jual</label>
                <input type="text" name="harga_jual" class="form-control" value="<?= @$hasil[0]->harga_jual ?>">
              </div>
              <div class="form-group">
                <label>Satuan</label>
                <input type="text" name="satuan" class="form-control" value="<?= @$hasil[0]->satuan ?>">
              </div>
             
              <div class="form-group">
                <label>Gambar</label>
                <input type="file" name="gambar" class="form-control">
              </div>
              <div class="form-group">
                <img src="<?= base_url('upload/'.@$hasil[0]->gambar) ?>" alt="">
              </div>
              
              <div class="form-group">
                <div class="float-right">
                  <button class="btn btn-success">Simpan</button>
                  <a href="#" onclick="return window.history.back();" class="btn btn-danger">Batal</a>
                </div>
              </div>
            </form>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
  <!-- /.content-wrapper -->