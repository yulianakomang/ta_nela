number_format(<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <?php $this->view('admin/parts/upper') ?>
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Data Produk</h1>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-12">

        <div class="card">
          <div class="card-header">
            <div class="float-lefts">
              <button data-toggle="modal" data-target="#tambahData" class="btn btn-success">Tambah Produk</button>            </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              
              <form action="" method="get" id="filter_kategori_produk">
                <div class="form-group">
                  <label>Pilih Kategori</label>
                  <select name="kategori_id" class="form-control" id="load_produk_kategori" style="width: 200px">
                    <option value="">-- Semua --</option>
                <?php foreach ($kategori_list as $cat): ?>
                  <option <?php if (@$_GET['kategori_id'] ==  $cat->id): ?>
                    selected
                  <?php endif ?> value="<?= $cat->id ?>"><?= $cat->nama_kategori ?></option>
                <?php endforeach ?>
                </select>
                </div>
              </form>


              <table class="table table-bordered table-striped load_datatables">
                <thead>
                  <tr>
                    <th>Kode Produk </th>
                    <th>Gambar </th>
                    <th>Kategori Barang</th>
                    <th>Nama produk</th>
                    <th>Harga pokok </th>
                    <th>Harga Jual</th>
                    <th>Satuan</th>
                    <th>Opsi</th>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach ($list as $item): ?>

                    <tr>

                      <td><?= $item->kode_produk ?></td>
                      <td><img src="<?= base_url('/uploads/'.$item->gambar) ?>" width="100px"></td>
                      <td>
                        <?php foreach ($kategori_list as $cat): ?>
                          <?php if ($cat->id == $item->kategori_id): ?>
                            <?= $cat->nama_kategori ?>
                          <?php endif ?>
                        <?php endforeach ?>
                      </td>
                      <td><?= $item->nama ?></td>
                      <td>Rp. <?= number_format($item->harga,0,'','.') ?></td>
                      <td>Rp. <?= number_format($item->harga_jual,0,'','.') ?></td>
                      <td><?= $item->satuan ?></td>
                      <td>
                        <a href="#" class="btn btn-sm btn-success edit_data" data-toggle="modal" data-target="#editData"
                            data-id="<?= $item->id ?>" 
                            data-kode="<?= $item->kode_produk ?>" 
                            data-nama="<?= $item->nama ?>" 
                            data-kategori_id="<?= $item->kategori_id ?>" 
                            data-harga="<?= number_format($item->harga,'0','','.') ?>" 
                            data-harga_jual="<?= number_format($item->harga_jual,'0','','.') ?>" 
                            data-satuan="<?= $item->satuan ?>" 
                            data-gambar="<?= $item->gambar ?>" >Ubah</a>
                        <!-- <a href="<?= base_url('admin/produk_hapus/'.$item->id) ?>" class="btn btn-sm btn-danger">Hapus</a>  -->
                        
                      </td>
                    </tr>
                  <?php endforeach ?>

                </tfoot>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Modal -->
  <div class="modal fade" id="tambahData" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <form action="<?= base_url('admin/produk_store') ?>" method="post" enctype="multipart/form-data">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel" style="text-align: center; margin:auto">Tambah Data Produk</h5>
          </div>
          <div class="modal-body">

            <div class="form-group">
              <label>Kode Produk</label>
              <input type="text" name="kode_produk" class="form-control" value="<?= 'BRG-'.date('Ymdhis') ?>" readonly>
            </div>
            <div class="form-group">
              <label>Kategori Barang</label>
              <select name="kategori_id" class="form-control">
                <?php foreach ($kategori_list as $cat): ?>
                  <option value="<?= $cat->id ?>"><?= $cat->nama_kategori ?></option>
                <?php endforeach ?>
              </select>
            </div>
            <div class="form-group">
              <label>Nama Produk</label>
              <input type="text" name="nama" class="form-control" oninvalid="InvalidMsg(this);" oninput="InvalidMsg(this);" required>
            </div>
            <div class="form-group">
              <label>Harga pokok</label>
              <input type="text" name="harga" class="form-control price-format" oninvalid="InvalidMsg(this);" oninput="InvalidMsg(this);" required>
            </div>
            <div class="form-group">
              <label>Harga Jual</label>
              <input type="text" name="harga_jual" class="form-control price-format" oninvalid="InvalidMsg(this);" oninput="InvalidMsg(this);" required>
            </div>
            <div class="form-group">
              <label>Satuan</label>
              <input type="text" name="satuan" class="form-control" oninvalid="InvalidMsg(this);" oninput="InvalidMsg(this);" required>
            </div>
            
            <div class="form-group">
              <label>Gambar</label>
              <input type="file" name="gambar" class="form-control">
            </div>
          </div>
          <div class="modal-footer">
            <button class="btn btn-primary">Simpan</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal" aria-label="Close">Batal</button>
          </div>
        </form>
      </div>
    </div>
  </div>

  <!-- Modal -->
  <div class="modal fade" id="editData" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <form action="<?= base_url('admin/produk_update') ?>" method="post" enctype="multipart/form-data">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel" style="text-align: center; margin:auto">Ubah Data Produk</h5>
            
          </div>
          <div class="modal-body">
            <input type="hidden" name="id" id="form_id">
            <div class="form-group">
              <label>Kode Produk</label>
              <input type="text" name="kode_produk" id="form_kode" class="form-control">
            </div>
             <div class="form-group">
              <label>Kategori Barang</label>
              <select name="kategori_id" id="form_kategori_id" class="form-control">
                <?php foreach ($kategori_list as $cat): ?>
                  <option value="<?= $cat->id ?>"><?= $cat->nama_kategori ?></option>
                <?php endforeach ?>
              </select>
            </div>
            <div class="form-group">
              <label>Nama Produk</label>
              <input type="text" name="nama" id="form_nama" class="form-control">
            </div>
            <div class="form-group">
              <label>Harga Pokok</label>
              <input type="text" name="harga" id="form_harga" class="form-control price-format">
            </div>
            <div class="form-group">
              <label>Harga Jual</label>
              <input type="text" name="harga_jual" id="form_harga_jual" class="form-control price-format">
            </div>
            <div class="form-group">
              <label>Satuan</label>
              <input type="text" name="satuan" id="form_satuan" class="form-control">
            </div>
           
            <div class="form-group">
              <label>Gambar</label>
              <input type="file" name="gambar" class="form-control">
            </div>
          </div>
          <div class="modal-footer">
            <button class="btn btn-primary">Simpan</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal" aria-label="Close">Batal</button>
          </div>
        </form>
      </div>
    </div>
  </div>

  <script src="<?= base_url('asset/') ?>plugins/jquery/jquery.min.js"></script>
  <script src="<?= base_url('asset/') ?>sweetalert2/dist/sweetalert2.all.js"></script>
<script type="text/javascript">

    <?php 
    $notif = @$this->session->flashdata('sweetalert');
    $opsi = @$this->session->flashdata('opsi');
    $mode = 'Ditambah!';
    if($opsi == 'tambah'){ $mode = 'Ditambah!';}elseif($opsi =='ubah'){ $mode = 'Diubah!';}else{$mode='dihapus!';}
    if($notif !== NULL){ ?>
      Swal.fire(
  'Berhasil!',
  'Data produk berhasil <?= $mode ?>',
  '<?= $notif ?>'
);
    <?php } ?>

    $('.edit_data').click(function () {
     var 
     id = $(this).data('id'),
     kode = $(this).data('kode'),
     nama = $(this).data('nama'),
     harga = $(this).data('harga'),
     harga_jual = $(this).data('harga_jual'),
     satuan = $(this).data('satuan'),
     kategori_id = $(this).data('kategori_id')
     ;
     

      $('#form_id').val(id);
      $('#form_kode').val(kode);
      $('#form_nama').val(nama);
      $('#form_harga').val(harga);
      $('#form_harga_jual').val(harga_jual);
      $('#form_satuan').val(satuan);
      $('#form_kategori_id').val(kategori_id);

      maskformat();
    });

    $('#load_produk_kategori').change(function(){
      $('#filter_kategori_produk').submit();
    });
</script>
