<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <?php $this->view('admin/parts/upper') ?>
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-12 text-center">
          <h1>Laporan Penjualan</h1>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-12">

        <div class="card">
          <div class="card-header">
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <form action="">
            <div class="row">
              <div class="col-md-4">
                <div class="form-group">
                  <label>Cari Data Tanggal</label>
                  <input type="date" name="tgl_start" class="form-control" value="<?= @$_GET['tgl_start'] ?>">
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label>Sampai Dengan</label>
                  <input type="date" name="tgl_end" class="form-control" value="<?= @$_GET['tgl_end'] ?>">
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label>Opsi</label><br>
                  <button class="btn btn-success"><i class="fa fa-search"></i> Preview</button>
                  <a href="#" class="btn btn-danger btn-cetak"><i class="fa fa-print"></i> Cetak</a>
                </div>
              </div>
            </div>
              </form>
            <br>
            <table class="table table-bordered table-striped load_datatables">
              <thead>
                <tr>
                  <th>TGL Order </th>
                  <th>No Faktur </th>                  
                  <th>Uang Muka </th>
                  <th>Sisa Pembayaran  </th>
                  <th>Nama Pelanggan</th>
                  <th>Total Harga</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach ($list as $item): ?>

                  <tr>

                    <td><?= date('d-m-Y',strtotime($item->tanggal_order ))?></td>
                    <td><?= $item->nomor_faktur ?></td> 
                    <td>Rp. <?= number_format($item->uang_muka,0,'','.') ?></td> 
                    <td>Rp. <?= number_format(@$func->getPembayaran($item->nomor_faktur)->sisa_pembayaran,0,'','.') ?></td>                  
                    <td><?php foreach ($pelanggan_list as $plg): ?>
                      <?php if ($plg->id == $item->id_pelanggan): ?>
                        <?= $plg->nama ?>
                      <?php endif ?>
                    <?php endforeach ?></td>
                    <td>Rp. <?= number_format($item->total,0,'','.') ?></td>
                    
                  </tr>
                <?php endforeach ?>
                
              </tfoot>
            </table>
            <br>
            <div class="row">
              <div class="col-md-4 offset-8">
                <div class="form-group">
                  <label>Total Penjualan</label>
                  <input type="text" class="form-control" readonly value="Rp. <?= ($func->getTotalPenjualan(@$_GET)===NULL)?'0':number_format($func->getTotalPenjualan(@$_GET)); ?>">
                </div>
                <div class="form-group">
                  <label>Total Harga Pokok</label>
                  <input type="text" class="form-control" readonly value="Rp. <?= ($func->getTotalPokok(@$_GET)===NULL)?'0':number_format($func->getTotalPokok(@$_GET)); ?>">
                </div>
                <div class="form-group">
                  <label>Total Keuntungan</label>
                  <input type="text" class="form-control" readonly value="Rp. <?= number_format($func->getTotalKeuntungan(@$_GET)) ?>">
                </div>
              </div>
            </div>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->


<script src="<?= base_url('asset/') ?>plugins/jquery/jquery.min.js"></script>
<script type="text/javascript">
   $('.btn-cetak').click(function(ev){
    ev.preventDefault();
    var crrurl = window.location.href;
    <?php if (@$_GET['tgl_start']) { ?>
    window.open(crrurl+'&cetak=1','_target');
    <?php }else{ ?>
    window.open(crrurl+'?cetak=1','_target');
    <?php } ?>
   });
</script>
