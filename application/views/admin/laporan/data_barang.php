<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <?php $this->view('admin/parts/upper') ?>
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-12 text-center">
          <h1>Laporan Barang Best Seller</h1>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-12">

        <div class="card">
          <div class="card-header">
            </div>
            <!-- /.card-header -->
            <div class="card-body">

              <form action="">
            <div class="row">
              <div class="col-md-3">
                <div class="form-group">
                  <label>Pilih Kategori</label>
                  <select name="kategori_id" class="form-control">
                    <option value="">-- Semua --</option>
                <?php foreach ($kategori_list as $cat): ?>
                  <option <?php if (@$_GET['kategori_id'] ==  $cat->id): ?>
                    selected
                  <?php endif ?> value="<?= $cat->id ?>"><?= $cat->nama_kategori ?></option>
                <?php endforeach ?>
                </select>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label>Cari Data Tanggal</label>
                  <input type="date" name="tgl_start" class="form-control" value="<?= @$_GET['tgl_start'] ?>">
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label>Sampai Dengan</label>
                  <input type="date" name="tgl_end" class="form-control" value="<?= @$_GET['tgl_end'] ?>">
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label>Opsi</label><br>
                  <button class="btn btn-success"><i class="fa fa-search"></i> Preview</button>
                </div>
              </div>
            </div>
              </form>
            <br>
              
              

              <table class="table table-bordered table-striped load_datatables_data_barang">
                <thead>
                  <tr>
                    <th>Kode Produk </th>
                    <th>Gambar </th>
                    <th>Nama Produk </th>
                    <th>Kategori </th>
                    <th>Terjual </th>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach ($list as $item): ?>

                    <tr>

                      <td><?= $item->kode_produk ?></td>
                      <td><img src="<?= base_url('/uploads/'.$item->gambar) ?>" width="100px"></td>
                      <td><?= $item->nama ?></td>
                      <td>
                        <?php foreach ($kategori_list as $cat): ?>
                          <?php if ($cat->id == $item->kategori_id): ?>
                            <?= $cat->nama_kategori ?>
                          <?php endif ?>
                        <?php endforeach ?>
                      </td>
                      <td><?= ($func->getTotalTerjual($item->id,@$_GET['tgl_start'],@$_GET['tgl_end']) == null)?'0':$func->getTotalTerjual($item->id,@$_GET['tgl_start'],@$_GET['tgl_end']); ?></td>
                    </tr>
                  <?php endforeach ?>

                </tfoot>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


  <script src="<?= base_url('asset/') ?>plugins/jquery/jquery.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
    $('.load_datatables_data_barang').DataTable( {
        "order": [[ 4, "desc" ]],
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true,
        "language": {
            "lengthMenu": "Menampilkan _MENU_ data per Halaman",
            "zeroRecords": "Maaf Tidak ada Data",
            "info": "Menampilkan Halaman _PAGE_ Dari _PAGES_",
            "infoEmpty": "Tidak terdapat data",
            "infoFiltered": "(mencari dari _MAX_ total data)",
            "search": "Cari Data",
            "paginate": {
                "first":      "Pertama",
                "last":       "Terakhir",
                "next":       "Selanjutnya",
                "previous":   "Sebelumnya"
            },
        },
      } );
  } );

    $('#load_produk_kategori').change(function(){
      $('#filter_kategori_produk').submit();
    });
</script>
