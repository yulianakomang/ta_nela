<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <?php $this->view('admin/parts/upper') ?>
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-12 text-center">
          <h1>Laporan Pelanggan </h1>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>
  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-12">

        <div class="card">
          <div class="card-header">
            
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <form action="">
            <div class="row">
              <div class="col-md-4"> 
                 <div class="form-group">
                  <label>Cari Data Tanggal</label>
                  <input type="date" name="tgl_start" class="form-control" value="<?= @$_GET['tgl_start'] ?>">
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label>Sampai Dengan</label>
                  <input type="date" name="tgl_end" class="form-control" value="<?= @$_GET['tgl_end'] ?>">
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label>Opsi</label><br>
                  <button class="btn btn-success"><i class="fa fa-search"></i> Preview</button>
                  <a href="#" class="btn btn-danger btn-cetak"><i class="fa fa-print"></i> Cetak</a>
                </div>
              </div>
            </div>
              </form>
            <br>
            <table class="table table-bordered table-striped load_datatables_pelanggan">
              <thead>
                <tr>
                  <th>No.</th>
                  <th>Kode Pelanggan </th>
                  <th>Nama Pelanggan </th>
                  <th>No Telpon </th>
                  <th>Alamat Pelanggan</th>
                  <th>Total Transaksi</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach ($list as $key => $item): ?>

                  <tr>

                    <td><?=($key+1)?></td>
                    <td><?= $item->kode ?></td>
                    <td><?= $item->nama ?></td>
                    <td><?= $item->no_telp ?></td>
                    <td><?= $item->alamat ?></td>
                    <td>Rp. <?= number_format($item->total, 0, '','.') ?></td>
                   
                  </tr>
                <?php endforeach ?>
                
              </tfoot>
            </table>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->




<script src="<?= base_url('asset/') ?>plugins/jquery/jquery.min.js"></script>
<script type="text/javascript">
    $('.btn-cetak').click(function(ev){
    ev.preventDefault();
    var crrurl = window.location.href;
    <?php if (@$_GET['tgl_start']) { ?>
    window.open(crrurl+'&cetak=1','_target');
    <?php }else{ ?>
    window.open(crrurl+'?cetak=1','_target');
    <?php } ?>
   });
    $(document).ready(function() {
    $('.load_datatables_pelanggan').DataTable( {
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true,
        "language": {
            "lengthMenu": "Menampilkan _MENU_ data per Halaman",
            "zeroRecords": "Maaf Tidak ada Data",
            "info": "Menampilkan Halaman _PAGE_ Dari _PAGES_",
            "infoEmpty": "Tidak terdapat data",
            "infoFiltered": "(mencari dari _MAX_ total data)",
            "search": "Cari Data",
            "paginate": {
                "first":      "Pertama",
                "last":       "Terakhir",
                "next":       "Selanjutnya",
                "previous":   "Sebelumnya"
            },
        },
      } );
  } );
</script>
