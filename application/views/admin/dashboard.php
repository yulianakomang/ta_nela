<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <?php $this->view('admin/parts/upper') ?>
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Menu Utama</h1>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          <div class="col-lg-4 col-6">
            <!-- small box -->
            <div class="small-box bg-danger">
              <div class="inner">
                 <h3><?= $total_pelanggan ?></h3>

                <p> Data Pelanggan</p>
              </div>
              <div class="icon">
                <i class="far fa-user nav-icon"></i>
              </div>
              <?php if ($this->session->userdata('level') === '0' ): ?>
              <a href="<?= base_url('admin/pelanggan') ?>" class="small-box-footer">Selengkapnya <i class="fas fa-arrow-circle-right"></i></a>
              <?php else: ?>
              <a href="<?= base_url('admin/laporan_pelanggan') ?>" class="small-box-footer">Selengkapnya <i class="fas fa-arrow-circle-right"></i></a>
              <?php endif ?>
            </div>
          </div>
          <div class="col-lg-4 col-6">
            <!-- small box -->
            <div class="small-box bg-info">
              <div class="inner">
                <h3><?= $total_penjualan ?></h3>

                <p> Data Penjualan</p>
              </div>
              <div class="icon">
                <i class="nav-icon fas fa-chart-pie"></i>
              </div>
              <?php if ($this->session->userdata('level') === '0' ): ?>
              <a href="<?= base_url('admin/penjualan') ?>" class="small-box-footer">Selengkapnya <i class="fas fa-arrow-circle-right"></i></a>
              <?php else: ?>
              <a href="<?= base_url('admin/laporan_penjualan') ?>" class="small-box-footer">Selengkapnya <i class="fas fa-arrow-circle-right"></i></a>
              <?php endif ?>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-4 col-6">
            <!-- small box -->
            <div class="small-box bg-success">
              <div class="inner">
                 <h3><?= $total_produk ?></h3>

                <p>Data Produk</p>
              </div>
              <div class="icon">
                <i class="nav-icon fas fa-copy"></i>
              </div>
              <?php if ($this->session->userdata('level') === '0' ): ?>
              <a href="<?= base_url('admin/produk') ?>" class="small-box-footer">Selengkapnya <i class="fas fa-arrow-circle-right"></i></a>
              <?php else: ?>
              <a href="<?= base_url('admin/laporan_data_barang') ?>" class="small-box-footer">Selengkapnya <i class="fas fa-arrow-circle-right"></i></a>
              <?php endif ?>
            </div>
          </div>
          <!-- ./col -->
          
          <!-- ./col -->
        </div>
        <!-- /.row -->
        
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->